$(document).ready(function() {
    $('.menu-hamburger').click(function(){
        $('.m-menu').slideToggle('fast');

        $(this).toggleClass('active');
    });

    $('select').niceSelect();
});