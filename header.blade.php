<!doctype html>
<html lang="ru">
<head>
	<meta charset="utf-8">	
	<title>Главная | Site Name</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link href="favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
	<script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
	<!--[if lt IE 10]> 
		<script src="js/html5shiv.js" type="text/javascript"></script>	
	<![endif]-->
</head>
<body>

<script>
	window.addEventListener('load', function() {
		setTimeout(function(){ 
			document.getElementsByTagName("body")[0].className += " active";
		}, 500);
	});
</script>
<style> 
@-webkit-keyframes mymove{from{top:0}50%{top:0px!important}to{top:70px}}@keyframes mymove{from{top:-30px}50%{top:0px!important}to{top:70px}}@-webkit-keyframes mymove{from{top:30px}50%{top:0px!important}to{top:-70px}}@keyframes mymove2{from{top:30px}50%{top:0px!important}to{top:-70px}}body.active .preloader{display:none}.preloader{position:fixed;top:0;left:0;width:100%;height:100%;z-index:9999999;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center}.preloader .preloaderOverlay{position:fixed;top:0;left:0;width:100%;height:100%;z-index:1;background-color:rgba(255,255,255,.99)}.preloader .logo{position:relative;z-index:1;overflow:hidden}.preloader .logo svg{max-width:250px;height:37px;}
</style>

<div class="preloader">
	<i class="preloaderOverlay"></i>
	<div class="logo">
		<svg xmlns="http://www.w3.org/2000/svg" width="810" height="120" viewBox="0 0 270 40">
			<defs>
				<style>
				  .cls-1 {
					fill: #01b4ff;
					fill-rule: evenodd;
				  }
				</style>
			</defs>
			<path id="лого" class="cls-1" d="M522.075,66.564l8.095,13.075,8.09-13.075,0.272-.44H545V85.936h-4.931V72.86l-8.177,12.655-0.273.421h-2.9l-0.272-.421L520.3,72.9V85.936H515.34V66.124H521.8ZM500.646,85.936h-0.027v0q-2.775,0-4.734-.187-0.62-.056-1.236-0.145a12.407,12.407,0,0,1-2-.44h-0.011a6.6,6.6,0,0,1-.7-0.271,5.626,5.626,0,0,1-.635-0.338,4.837,4.837,0,0,1-2.136-2.82l0-.013c-0.052-.175-0.095-0.353-0.13-0.533-0.039-.2-0.072-0.409-0.1-0.613a38.283,38.283,0,0,1-.224-4.769q0-2.189.1-3.655a11.256,11.256,0,0,1,.329-2.269l0-.007h0A5.568,5.568,0,0,1,489.4,69.2a5.13,5.13,0,0,1,.333-0.626,5.212,5.212,0,0,1,2.919-2.159v0a15.622,15.622,0,0,1,3.267-.6q2-.194,4.732-0.194v0q2.78,0,4.74.186a15.418,15.418,0,0,1,3.242.591l0,0h0v0a6.426,6.426,0,0,1,.692.271,5.5,5.5,0,0,1,.638.341,4.933,4.933,0,0,1,2.151,2.836v0a11.4,11.4,0,0,1,.355,2.317q0.108,1.468.108,3.632t-0.1,3.63a11.187,11.187,0,0,1-.332,2.272h0a4.959,4.959,0,0,1-1.266,2.161h0a5.486,5.486,0,0,1-2.213,1.3l-0.011,0q-0.361.116-.771,0.21t-0.879.172a40.93,40.93,0,0,1-6.36.39h0Zm0-4.707a40.828,40.828,0,0,0,4.578-.2,4.4,4.4,0,0,0,1.737-.409,2.5,2.5,0,0,0,.428-1.249,21.53,21.53,0,0,0,.228-3.57,21.938,21.938,0,0,0-.229-3.59,2.572,2.572,0,0,0-.418-1.256h0a4.47,4.47,0,0,0-1.736-.4,40.966,40.966,0,0,0-4.59-.2v0a40.875,40.875,0,0,0-4.593.2,4.374,4.374,0,0,0-1.731.406l0,0h0a0.4,0.4,0,0,0-.062.081v0a1.673,1.673,0,0,0-.135.3,14.915,14.915,0,0,0-.447,4.462,21.424,21.424,0,0,0,.229,3.56,2.523,2.523,0,0,0,.429,1.255h0a0.579,0.579,0,0,0,.14.075,3.077,3.077,0,0,0,.436.137,27.321,27.321,0,0,0,5.734.4h0ZM469.224,75.8q0,4.257.626,4.82l0,0v0a0.534,0.534,0,0,0,.127.076,2.5,2.5,0,0,0,.37.132,20.658,20.658,0,0,0,4.952.4c1.591,0,2.845-.033,3.755-0.1a6.5,6.5,0,0,0,1.511-.21,0.717,0.717,0,0,0,.114-0.087h0a0.934,0.934,0,0,0,.139-0.163,4.675,4.675,0,0,0,.48-2.5V77.246h4.96v0.931h0a13.411,13.411,0,0,1-.5,3.98c-0.059.186-.121,0.358-0.185,0.513a4.054,4.054,0,0,1-1.559,1.98h0l-0.006,0h0a4.6,4.6,0,0,1-.745.371,7.7,7.7,0,0,1-.883.288,29.907,29.907,0,0,1-7.081.622,30.241,30.241,0,0,1-5.531-.418,7.132,7.132,0,0,1-3.415-1.42,6,6,0,0,1-1.623-3.18,23.019,23.019,0,0,1-.471-5.118,23.334,23.334,0,0,1,.468-5.134,6.076,6.076,0,0,1,1.606-3.2,7.045,7.045,0,0,1,3.419-1.433,30.1,30.1,0,0,1,5.547-.418v0h0.02v0a40.755,40.755,0,0,1,5.387.29c0.27,0.038.5,0.072,0.676,0.1a7.526,7.526,0,0,1,2.645.878l0.021,0.011,0,0,0,0a3.6,3.6,0,0,1,1.036,1.03,5.6,5.6,0,0,1,.68,1.449,13.514,13.514,0,0,1,.5,4v0.923h-4.96V73.384q0-2.337-.543-2.61h0a5.636,5.636,0,0,0-1.657-.3q-1.387-.125-3.8-0.124-4.8,0-5.458.6h0a2.688,2.688,0,0,0-.4,1.269,23.05,23.05,0,0,0-.217,3.587h0Zm-25.562-9.676h18.123v4.707H447.7v2.719h13.6v4.707H447.7v2.97h14.082v4.707h-19.04V66.125h0.917Zm-20.521,0h4.014v15.1h13.372v4.7h-18.3V66.124h0.917Zm-21.684,0H419.58v4.707H405.493v2.719h13.6v4.707H405.5v2.97H419.58v4.707H400.54V66.125h0.917Zm-22.853,0h19.651v4.707h-7.793v15.1H385.5v-15.1h-7.815V66.124H378.6m-43.041,13.3a2.913,2.913,0,0,0,.173,1.137,0.572,0.572,0,0,0,.285.313,6.875,6.875,0,0,0,1.7.253q1.416,0.1,3.826.1h0.041v0q2.361,0,3.83-.108c0.144-.01.288-0.022,0.432-0.034h0a5.242,5.242,0,0,0,1.252-.216v0a0.571,0.571,0,0,0,.27-0.313,2.941,2.941,0,0,0,.174-1.142V65.869H352.5V79.433h0a11.112,11.112,0,0,1-.4,3.267c-0.053.165-.1,0.305-0.151,0.42a3.073,3.073,0,0,1-1.306,1.578,8.888,8.888,0,0,1-3.407.953,46.714,46.714,0,0,1-5.694.282v0h-0.027v0a53.5,53.5,0,0,1-5.6-.235q-0.663-.07-1.32-0.189a6.239,6.239,0,0,1-1.932-.617,3.733,3.733,0,0,1-1.613-2.132,10.49,10.49,0,0,1-.45-3.337V65.869h4.961V79.422Zm19.564-9.513V66.125h20.291V70.2l-0.331.278L362.3,81.229h13.114v4.708H354.539V81.859l0.334-.279L367.81,70.833H355.127V69.91M275,80.5a4.69,4.69,0,0,0,0-9,5.648,5.648,0,0,1,0,9h0Zm1.659,2.386a6.889,6.889,0,1,0-.844-13.726,6.89,6.89,0,0,1,0,13.674,6.917,6.917,0,0,0,.844.052h0Zm2.412,3.988a10.877,10.877,0,0,0,0-21.754,10.5,10.5,0,0,0-1.332.084,10.878,10.878,0,0,1,0,21.587,10.765,10.765,0,0,0,1.332.082M294.5,96a20,20,0,1,0-2.452-39.856,20.008,20.008,0,0,1,0,39.706A20.328,20.328,0,0,0,294.5,96Zm-11.947-5.379a14.624,14.624,0,1,0-1.792-29.137,14.626,14.626,0,0,1,0,29.026,14.612,14.612,0,0,0,1.792.111h0Zm4.891,3.252a17.876,17.876,0,0,0,0-35.752,18.066,18.066,0,0,0-2.191.136,17.879,17.879,0,0,1,0,35.481,18.006,18.006,0,0,0,2.191.135" transform="translate(-275 -56)"/>
		</svg>
	</div>
</div>

<i class="overlayForMobileMenu"></i>
<div class="wrapper">

<div class="mobileMenu">
	<div class="wraps">
		<nav class="icons">
			<ul>
				<li>
					<a href="javascript:void(0);"><span class="icon-earth"></span></a>
					<ul>
						<li><a href="#">Андижанская область</a></li>
						<li><a href="#">Бухарская область</a></li>
						<li><a href="#">Джизакская область</a></li>
						<li><a href="#">Кашкадарьинская область</a></li>
						<li><a href="#">Навоийская область</a></li>
						<li><a href="#">Наманганская область</a></li>
						<li><a href="#">Республика Каракалпакстан</a></li>
						<li><a href="#">Самаркандская область</a></li>
						<li><a href="#">Сурхандарьинская область</a></li>
						<li><a href="#">Сырдарьинская область</a></li>
						<li><a href="#">Ферганская область</a></li>
						<li><a href="#">Хорезмская область</a></li>									
					</ul>
				</li>
				<li><a href="javascript:void(0);"><span class="icon-user"></span></a></li>
				<li>
					<a href="javascript:void(0);">Русский</a>
					<ul class="lang">
						<li><a href="#">English</a></li>
						<li><a href="#">O`ZB</a></li>
					</ul>
				</li>
			</ul>
		</nav>
		<nav class="menu">
			<ul>
				<li class="active"><a href="#">Частным лицам</a></li>
				<li><a href="#">Бизнесу</a></li>
				<li><a href="#">Операторам</a></li>
			</ul>
		</nav>
		<div class="search">
			<form action="#" method="POST">
				<div class="search__wraps">
					<input type="text" name="search" value="" placeholder="Введите поисковый запрос" />
					<button type="submit" class="btn"><span class="icon-search"></span></button>
				</div>
			</form>
		</div>
		<nav class="mainMenu">
			<ul>
				<li class="dropdown">
					<a href="#">Мобильная связь</a>
					<div class="subMenu">
						<ul>
							<li><a href="#">GSM</a></li>
							<li><a href="#">CDMA</a></li>
						</ul>
					</div>
				</li>
				<li><a href="#">Интернет</a></li>
				<li><a href="#">Телефон в офис</a></li>
				<li><a href="#">IPTV</a></li>
				<li><a href="#">Облачные сервисы</a></li>
				<li><a href="#">Другие услуги</a></li>
				<li class="dropdown">
					<a href="#">Частным клиентам</a>
					<div class="subMenu">
						<ul>
							<li><a href="#">Тарифы GSM</a></li>
							<li><a href="#">Тарифы CDMA</a></li>
							<li><a href="#">Мобильный интернет</a></li>
							<li><a href="#">Домашний интернет</a></li>
							<li><a href="#">Интерактивное ТВ</a></li>
							<li><a href="#">Как подключиться</a></li>
							<li><a href="#">Мобильные телефоны</a></li>
							<li><a href="#">Прочие услуги</a></li>
						</ul>
					</div>
				</li>
				<li class="dropdown">
					<a href="#">Бизнесу</a>
					<div class="subMenu">
						<ul>
							<li><a href="#">Тарифы GSM</a></li>
							<li><a href="#">Тарифы CDMA</a></li>
							<li><a href="#">Мобильный интернет</a></li>
							<li><a href="#">Интернет в офис</a></li>
							<li><a href="#">Интерактивное ТВ</a></li>
							<li><a href="#">Как подключиться</a></li>
							<li><a href="#">Мобильные телефоны и оборудование</a></li>
							<li><a href="#">Прочие услуги</a></li>
						</ul>
					</div>
				</li>
				
				<li class="dropdown">
					<a href="#">Операторам</a>
					<div class="subMenu">
						<ul>
							<li><a href="#">Аренда транспортных каналов</a></li>
							<li><a href="#">Фиксированная телефония</a></li>
							<li><a href="#">Доступ к сети интернет</a></li>
							<li><a href="#">Облачные сервисы</a></li>
							<li><a href="#">Телеграфная связь</a></li>
							<li><a href="#">Прочие услуги</a></li>
						</ul>
					</div>
				</li>
				
				<li class="dropdown">
					<a href="#">АК «Узбектелеком»</a>
					<div class="subMenu">
						<ul>
							<li><a href="#">О компании</a></li>
							<li><a href="#">Копоративное управление</a></li>
							<li><a href="#">Пресс-центр</a></li>
							<li><a href="#">Вакансии</a></li>
							<li><a href="#">Закупки</a></li>
							<li><a href="#">Офисы и филиалы</a></li>
							<li><a href="#">Правовая информация</a></li>
							<li><a href="#">Публичная оферта</a></li>
						</ul>
					</div>
				</li>
			</ul>
		</nav>
	</div>
</div>

<header>
	<div class="col-top">
		<div class="container">
			<div class="wrap">
				<div class="col-left">
					@if( $gSections )
					<nav>
						<ul>
							@foreach( $gSections as $gSection )
							<li class="">
								<a href="{{ $gSection->getLink($gLocale) }}">{{ $gSection->title }}</a>
							</li>
							@endforeach
						</ul>
					</nav>
					@endif
				</div>
				<div class="col-middle specialFeatured">
					<a href="#" class="specialFeaturedBtn"><span class="icon-glasses"></span> Специальные возможности</a>
					<div class="specialFeaturedBlock">
						<div class="specialFeaturedBlockWraps">
							<span>Вид:</span>
							<div class="typeView">
								<a href="javascript:void(0);" class="default active">A</a>
								<a href="javascript:void(0);" class="blackWhite">A</a>
							</div>
							<span>Размер шрифта:</span>
							<div class="typeSizeFonts">
								<ul>
									<li class="inc1">
										<input type="radio" id="inc1" name="selector">
										<label for="inc1">A</label>
										<div class="check"></div>
									</li>					
									<li class="inc2">
										<input type="radio" id="inc2" name="selector">
										<label for="inc2">A</label>
										<div class="check"></div>
									</li>					
									<li class="inc3">
										<input type="radio" id="inc3" name="selector">
										<label for="inc3">A</label>
										<div class="check"></div>
									</li>					
									<li class="inc4">
										<input type="radio" id="inc4" name="selector">
										<label for="inc4">A</label>
										<div class="check"></div>
									</li>					
									<li class="inc5">
										<input type="radio" id="inc5" name="selector">
										<label for="inc5">A</label>
										<div class="check"></div>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-right">
					<ul class="regionLangbtns">
						<li>
							<a href="#">
								<span class="icon-earth"></span> 
								Ташкент и область
							</a>
							<ul>
								<li><a href="#">Андижанская область</a></li>
								<li><a href="#">Бухарская область</a></li>
								<li><a href="#">Джизакская область</a></li>
								<li><a href="#">Кашкадарьинская область</a></li>
								<li><a href="#">Навоийская область</a></li>
								<li><a href="#">Наманганская область</a></li>
								<li><a href="#">Республика Каракалпакстан</a></li>
								<li><a href="#">Самаркандская область</a></li>
								<li><a href="#">Сурхандарьинская область</a></li>
								<li><a href="#">Сырдарьинская область</a></li>
								<li><a href="#">Ферганская область</a></li>
								<li><a href="#">Хорезмская область</a></li>									
							</ul>
						</li>
						<li>
							@if( isset($gCurrentLanguage) )
							<a href="#">{{ $gCurrentLanguage->title }}</a>
							@endif
							@if( isset($gLanguages) )
							<ul class="lang">
								@foreach($gLanguages as $gLanguage)
									<?php 
										if( isset($languageSelectorLinks[$gLanguage->locale]) ) {
					                        $localizedUrl = $languageSelectorLinks[$gLanguage->locale];
					                    } else {
					                        $localizedUrl = LaravelLocalization::getLocalizedURL($gLanguage->locale, Request::url());
				                    } ?>
									<li><a href="{{ $localizedUrl }}">{{ $gLanguage->title }}</a></li>
								@endforeach
							</ul>
							@endif
						</li>
					</ul>
					<a href="#" class="myCabinetBtn"><span class="icon-user"></span> Мой Узтелеком</a>
				</div>
			</div>
		</div>
	</div>
	<div class="col-bottom">
		<div class="container">
			<div class="wrap">
				<div class="col-left">
					<a href="/">
						{{ Html::image('images/logo.svg') }}
					</a>
				</div>
				<div class="col-right">
					<nav>
						<ul>
							<li class="dropdown">
								<a href="#">Мобильная связь</a>
								<div class="subMenu">
									<ul>
										<li class="dropdown">
											<a href="#">GSM</a>
											<ul>
												<li><a href="#">Тарифы</a></li>
												<li class="dropdown">
													<a href="#">Услуги</a>
													<ul>
														<li><a href="#">Услуга 1</a></li>
														<li><a href="#">Услуга 2</a></li>
														<li><a href="#">Услуга 3</a></li>
														<li><a href="#">Услуга 4</a></li>
													</ul>
												</li>
												<li><a href="#">Пакеты</a></li>
												<li><a href="#">Акции</a></li>
												<li><a href="#">Телефоны и оборудование</a></li>
												<li><a href="#">Золотые номера</a></li>
												<li><a href="#">Полезная информация</a></li>
												<li class="dropdown">
													<a href="#">Роуминг</a>
													<ul>
														<li><a href="#">Услуга 1</a></li>
														<li><a href="#">Услуга 2</a></li>
														<li><a href="#">Услуга 3</a></li>
														<li><a href="#">Услуга 4</a></li>
													</ul>
												</li>
												<li><a href="#">Международная связь</a></li>
												<li><a href="#">Развлечения</a></li>
											</ul>
										</li>
										<li class="dropdown">
											<a href="#">CDMA</a>
											<ul>
												<li><a href="#">Тарифы</a></li>
												<li class="dropdown">
													<a href="#">Услуги</a>
													<ul>
														<li><a href="#">Услуга 1</a></li>
														<li><a href="#">Услуга 2</a></li>
														<li><a href="#">Услуга 3</a></li>
														<li><a href="#">Услуга 4</a></li>
													</ul>
												</li>
												<li><a href="#">Пакеты</a></li>
												<li><a href="#">Акции</a></li>
												<li><a href="#">Телефоны и оборудование</a></li>
												<li><a href="#">Золотые номера</a></li>
												<li><a href="#">Полезная информация</a></li>
												<li class="dropdown">
													<a href="#">Роуминг</a>
													<ul>
														<li><a href="#">Услуга 1</a></li>
														<li><a href="#">Услуга 2</a></li>
														<li><a href="#">Услуга 3</a></li>
														<li><a href="#">Услуга 4</a></li>
													</ul>
												</li>
												<li><a href="#">Международная связь</a></li>
												<li><a href="#">Развлечения</a></li>
											</ul>
										</li>
									</ul>
								</div>
							</li>
							<li><a href="#">Интернет</a></li>
							<li><a href="#">Телефон в офис</a></li>
							<li><a href="#">IPTV</a></li>
							<li><a href="#">Облачные сервисы</a></li>
							<li><a href="#">Другие услуги</a></li>
						</ul>
						<div class="searchForm">
							<form action="#" method="POST">
								<div class="wraps">
									<input type="text" name="search" value="" placeholder="Введите поисковый запрос" />
									<button type="submit" class="btn"><span class="icon-search"></span></button>
								</div>
							</form>
						</div>
					</nav>
					<a href="#" class="searchBtn">
						<span class="icon-search"></span>
					</a>
					<div class="hamburger hamburger--slider js-hamburger">
						<div class="hamburger-box">
							<div class="hamburger-inner"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>