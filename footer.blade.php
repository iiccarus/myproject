<test></test>

<div class="emptyblock"></div>	
	<test></test>
</div>

<footer>
	<div class="container">
		<div class="wrap">
			<div class="col-top">
				<div class="col-left">
					<div class="wraps">
						<nav>
							<h3>Частным клиентам</h3>
							<ul>
								<li><a href="#">Тарифы GSM</a></li>
								<li><a href="#">Тарифы CDMA</a></li>
								<li><a href="#">Мобильный интернет</a></li>
								<li><a href="#">Домашний интернет</a></li>
								<li><a href="#">Интерактивное ТВ</a></li>
								<li><a href="#">Как подключиться</a></li>
								<li><a href="#">Мобильные телефоны</a></li>
								<li><a href="#">Прочие услуги</a></li>
							</ul>
						</nav>
						<nav>
							<h3>Бизнесу</h3>
							<ul>
								<li><a href="#">Тарифы GSM</a></li>
								<li><a href="#">Тарифы CDMA</a></li>
								<li><a href="#">Мобильный интернет</a></li>
								<li><a href="#">Интернет в офис</a></li>
								<li><a href="#">Интерактивное ТВ</a></li>
								<li><a href="#">Как подключиться</a></li>
								<li><a href="#">Мобильные телефоны и оборудование</a></li>
								<li><a href="#">Прочие услуги</a></li>
							</ul>
						</nav>
						<nav>
							<h3>Операторам</h3>
							<ul>
								<li><a href="#">Аренда транспортных каналов</a></li>
								<li><a href="#">Фиксированная телефония</a></li>
								<li><a href="#">Доступ к сети интернет</a></li>
								<li><a href="#">Облачные сервисы</a></li>
								<li><a href="#">Телеграфная связь</a></li>
								<li><a href="#">Прочие услуги</a></li>
							</ul>
						</nav>
						<nav>
							<h3>АК «Узбектелеком»</h3>
							<ul>
								<li><a href="#">О компании</a></li>
								<li><a href="#">Копоративное управление</a></li>
								<li><a href="#">Пресс-центр</a></li>
								<li><a href="#">Вакансии</a></li>
								<li><a href="#">Закупки</a></li>
								<li><a href="#">Офисы и филиалы</a></li>
								<li><a href="#">Правовая информация</a></li>
								<li><a href="#">Публичная оферта</a></li>
							</ul>
						</nav>
					</div>
				</div>
				<div class="col-right">
					<div class="wraps">
						<h3>Свяжитесь с нами</h3>
						<ul class="contact">
							<li>
								<span class="icon icon-phone-0"></span>
								<a href="tel:1084" class="phone">1084</a>
								<span class="note">Единый центр обслуживания вызовов</span>
							</li>
							<li>
								<span class="icon icon-mobile"></span>
								<a href="tel:+998712443443" class="phone"><span>+998 71</span> 244 34 43</a>
								<span class="note">с 9:00 до 18:00 (перерыв с 13:00 до 14:00) понедельник-пятница</span>
							</li>
							<li>
								<span class="icon icon-email"></span>
								<a href="#">Отправить обращение</a>
							</li>
							<li>
								<span class="icon icon-et"></span>
								<a href="mailto:info@uztelecom.uz">info@uztelecom.uz</a>
								<span class="note"></span>
							</li>
						</ul>
						<ul class="socialLinks">
							<li><a href="#"><span class="icon-fb"></span></a></li>
							<li><a href="#"><span class="icon-tw"></span></a></li>
							<li><a href="#"><span class="icon-yotube"></span></a></li>
							<li><a href="#"><span class="icon-tg"></span></a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-bottom">
				<p>© 2001—2018 Узбектелеком. Все услуги лицензированы</p>
			</div>
		</div>
	</div>
</footer>

<link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css">	
<script src="{{ asset('js/ion.rangeSlider.min.js') }}"></script>
<script src="{{ asset('js/script.js') }}"></script>

@yield('script')

</body>
</html>
